@extends('layouts.layout')
@section('content')

    <form action="{{ route('comment.store') }}" method="POST" role="form">
        {{ csrf_field() }}
        <legend>افزودن دیدگاه جدید:</legend>
        <input type="hidden" name="post_id" value="3" >

        <div class="form-group">
            <input type="text" title="text" class="form-control" name="name" id="" placeholder="نام شما...">
        </div>
        <div class="form-group">
            <textarea type="text" title="text" class="form-control" name="text" id="" placeholder="دیدگاه..."></textarea>
        </div>
        <div>
            <button type="submit" class="btn btn-success">انتشار</button>
            <a href="{{ route('comment.index') }}" class="btn btn-danger">لغو</a>
        </div>
    </form>

@endsection
