@extends('layouts.layout')
@section('content')

    <form action="{{ route('comment.update', $comment->id) }}" method="POST" role="form">
        {{ csrf_field() }}
        {{ method_field('PUT') }}
    	<legend>ویرایش  دیدگاه</legend>

		<div class="form-group">
			<input type="text" title="text" class="form-control" value="{{ $comment->post_id }}" name="category_id" id="" placeholder="نام دسته...">
		</div>
		<div class="form-group">
			<input type="text" title="text" class="form-control" value="{{ $comment->name }}" name="title" id="" placeholder="عنوان...">
		</div>
		<div class="form-group">
			<textarea type="text" title="text" class="form-control" name="text" id="" >{{ $comment->text }}</textarea>
		</div>

    	<button type="submit" class="btn btn-success">ثبت ویرایش</button>
    </form>

@endsection
