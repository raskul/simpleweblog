@extends('layouts.layout')
@section('content')

    <table class="table table-hover">
        <thead>
        <tr>
            <th>متن دیدگاه</th>
            <th>نویسنده</th>
            <th>پست</th>
            <th>عملیات:</th>
        </tr>
        </thead>
        <tbody>

        @foreach($comments as $comment)
            @can('edit-comment', $comment)
                <tr>
                    <td>{{ $comment->text }}</td>
                    <td>{{ $comment->name }}</td>
                    <td><a href="{{route('post.show' ,$comment->post->id)}}">{{ $comment->post->title }}</a></td>
                    <td>
                        <a href="{{ route('comment.edit', $comment->id) }}" class="btn btn-default btn-sm">
                            <span class="glyphicon glyphicon-edit"></span>
                        </a>
                        <form style="display: inline" action="{{ route('comment.destroy', $comment->id) }}"
                              method="post">
                            {{ csrf_field() }}
                            {{ method_field('DELETE') }}
                            <button type="submit" class="btn btn-default btn-sm">
                                <span class="glyphicon glyphicon-trash"></span>
                            </button>
                        </form>
                    </td>
                </tr>
            @endcan
        @endforeach

        </tbody>
    </table>

@endsection
