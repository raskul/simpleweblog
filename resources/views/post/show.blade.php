@extends('layouts.layout')
@section('content')
    @include('partials.header')

    @can('edit-post', $post)
        <div style="color: #831700; font-size:large" class="pull-left">
            <a href="{{route('post.edit', $post->id)}}">
                ویرایش پست
            </a>
        </div>
    @endcan
    <div class="col-md-12" align="center">
        <img src="/{{ $post->image }}">
    </div>

    <div class="container post">
        <div class="row">
            <div class="col-md-12 post-title">
                <h1>{{ $post->title }}</h1>
                <p class="author"><strong>{{ $post->user->name }}</strong> <span
                            class="text-muted">اخرین ویرایش: {{ $post->updated_at }} </span></p>
            </div>
            <div class="col-md-12 col-md-offset-0 post-body">
                {!! $post->text !!}
            </div>
        </div>
        <hr>
        <div class="row">
            <div class="col-md-6">
                <h2>دیدگاه ها:</h2>
                @if($post->comments == '[]')
                    <div style="font-size: small; color: #5a5a5a">دیدگاهی برای این نوشته وجود ندارد.</div>
                @else
                    @foreach($post->comments as $comment)
                        <span style="color: #2d3a83; font-size:small">{{ $comment->name }}
                            میگه:
                        </span>
                        @can('edit-comment', $comment)
                            <span style="color: #831700; font-size:xx-small" class="pull-left">
                                <a href="{{route('comment.edit', $comment->id)}}">
                                ویرایش
                                </a>
                            </span>
                            {{--<br>--}}
                        @endcan
                        <div style="margin-right: 50px; border-bottom: 0.5px dashed rgba(0,0,0,0.17)">{!! $comment->text !!}</div>
                    @endforeach
                @endif
                <hr>
                <h2>دسته بندی:</h2>
                <div><a href="{{route('category.index')}}/{{$post->category->id}}">{{$post->category->name}}</a></div>
                <hr>
                <h2>برچسب ها:</h2>
                @foreach($post->tags as $tag)
                    <div><a href="{{route('tag.index')}}/{{ $tag->id }}">{{ $tag->name }}</a></div>
                @endforeach
            </div>

            <div class="col-md-6">


                {!! Form::open(['route'=>['comment.store', 'post_id' => $post->id ], 'method' => 'post']) !!}

                @if(Auth::check())
                    <div>
                            <span style="color: rgba(90,90,90,0.48)">
                                وارد شده با نام کاربری:
                            </span>
                        <span>{{ auth()->user()->name }}</span>
                    </div>

                @else

                <!-- فیلد نام شما -->
                    <div class="form-group {{ $errors->has('name') ? ' has-error' : '' }}">
                        {!! Form::label('name', 'نام شما:') !!}
                        {!! Form::text('name', null, ['class' => 'form-control']) !!}
                        @if ($errors->has('name'))
                            <span class="help-block"><strong>{{ $errors->first('name') }}</strong></span>
                        @endif
                    </div>
                @endif

            <!-- فیلد افزودن دیدگاه: -->
                <div class="form-group {{ $errors->has('text') ? ' has-error' : '' }}">
                    {!! Form::label('text', 'افزودن دیدگاه:') !!}
                    <br>
                    {!! Form::textarea('text', null, ['class' => 'form-control', 'id'=> 'editorcm']) !!}
                    @if ($errors->has('text'))
                        <span class="help-block"><strong>{{ $errors->first('text') }}</strong></span>
                    @endif
                </div>
                <button class="btn btn-success">ثبت دیدگاه</button>
                {!! Form::close() !!}

            </div>
        </div>
    </div>

    @push('footer')
        <script type="text/javascript" src="{{ asset('editor/basic/ckeditor/ckeditor.js') }}"></script>
        <script type="text/javascript" src="{{ asset('editor/basic/ckeditor/adapters/jquery.js') }}"></script>
        <script>
            $(document).ready(function () {
                $('textarea#editorcm').ckeditor();
            });
        </script>
    @endpush
    @include('partials.footer')
@endsection
