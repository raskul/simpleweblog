@extends('layouts.layout')
@section('content')
    <div class="col-md-8 col-md-offset-2">

        {!! Form::model($post, ['route'=>['post.update', $post->id ], 'method' => 'patch', 'files' => true ]) !!}
        <h2>ویرایش پست:</h2>

        @include('post.fields')

        {!! Form::close() !!}

    </div>

    @push('footer')
        <script type="text/javascript" src="{{ asset('editor/full/ckeditor/ckeditor.js') }}"></script>
        <script type="text/javascript" src="{{ asset('editor/full/ckeditor/adapters/jquery.js') }}"></script>
        <script>
            $( document ).ready( function() {
                $( 'textarea#editor1' ).ckeditor();
            } );
        </script>
    @endpush
@endsection
