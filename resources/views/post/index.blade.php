@extends('layouts.layout')
@section('content')

    <a href="{{ route('post.create') }}" class="btn btn-primary">اضافه کردن پست جدید</a>
    <hr>
    <table class="table table-hover">
        <thead>
        <tr>
            <th>عنوان</th>
            <th>نویسنده</th>
            <th>دسته بندی</th>
            <th>نمایش نوشته</th>
            <th>عملیات:</th>
        </tr>
        </thead>
        <tbody>

        @foreach($posts as $post)
{{--            @can('edit-post', $post)--}}
                <tr>
                    <td>{{ $post->title }}</td>
                    <td>{{ $post->user->name }}</td>
                    <td>{{ $post->category->name }}</td>
                    <td><a href="{{ route('post.show', $post->id) }}">بیشتر...</a></td>

                    <td>
                        <a href="{{ route('post.edit', $post->id) }}" class="btn btn-default btn-sm">
                            <span class="glyphicon glyphicon-edit"></span>
                        </a>
                        <form style="display: inline" action="{{ route('post.destroy', $post->id) }}" method="post">
                            {{ csrf_field() }}
                            {{ method_field('DELETE') }}
                            <button type="submit" class="btn btn-default btn-sm">
                                <span class="glyphicon glyphicon-trash"></span>
                            </button>
                        </form>
                    </td>
                </tr>
            {{--@endcan--}}
        @endforeach

        </tbody>
    </table>

@endsection
