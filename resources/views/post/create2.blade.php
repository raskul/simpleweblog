@extends('layouts.layout')
@section('content')
    <div class="col-md-8 col-md-offset-2">
        @if($errors)
            <ul>
                @foreach ($errors->all() as $error)
                    <li class="alert alert-danger">{{ $error }}</li>
                @endforeach
            </ul>
        @endif
        {!! Form::open(['route' => 'post.store', 'files' => true]) !!}
        <h2>افزودن پست جدید:</h2>

            @can('create', \App\Post::class)

            @include('post.fields')

            @endcan

        {!! Form::close() !!}
    </div>

    @push('footer')
        <script type="text/javascript" src="{{ asset('editor/full/ckeditor/ckeditor.js') }}"></script>
        <script type="text/javascript" src="{{ asset('editor/full/ckeditor/adapters/jquery.js') }}"></script>
        <script>
            $( document ).ready( function() {
                $( 'textarea#editor1' ).ckeditor();
            } );
        </script>
    @endpush
@endsection
