@extends('layouts.layout')
@section('content')

    @if(Session::has('title_massage'))
        @include('partials.flash')
    @endif

    <form action="{{ route('post.store') }}" method="POST" role="form" enctype="multipart/form-data">
        {{ csrf_field() }}
        <legend>افزودن پست جدید:</legend>
        <input type="hidden" name="user_id" value="1">

        <div class="form-group">
            <select name="category_id" class="form-control">
                @foreach( $categories as $category_id => $category)
                    <option value="{{ $category_id }}"> {{ $category }}</option>
                @endforeach
            </select>
        </div>

        <div class="form-group">
            <input type="text" title="title" class="form-control" name="title" id=""
                   value="@if(Session::has('title')) {{ Session::get('title') }} @endif" placeholder="عنوان...">
        </div>
        <div class="form-group">
        <textarea type="text" title="text" class="form-control" name="text" id=""
                  value="@if(Session::has('text')) {{ Session::get('text') }} @endif" placeholder="متن..."></textarea>
        </div>

        <div class="form-group">
            <select name="tag_id[]" class="form-control" multiple>
                <option></option>
                @foreach( $tags as $tag)
                    <option value="{{ $tag->id }}"> {{ $tag->name }}</option>
                @endforeach
            </select>
        </div>

        <div class="form-group">
            <span>عکس پست:</span>
            <input type="file" name="image" title="تصویر شاخص"
                   value="@if(Session::has('image')) {{ Session::get('image') }} @endif">
        </div>

        <div>
            <button type="submit" class="btn btn-success">انتشار</button>
            <a href="{{ route('post.index') }}" class="btn btn-danger">لغو</a>
            <a href="{{ route('tag.create') }}" class="btn btn-default">ساختن تگ جدید</a>
        </div>
    </form>

@endsection
