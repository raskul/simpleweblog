<!-- فیلد دسته -->
<div class="form-group {{ $errors->has('category_id') ? ' has-error' : '' }}">
    {!! Form::label('category_id', 'دسته') !!}
    {!! Form::select('category_id', $categories, null, ['class' => 'form-control']) !!}
    @if ($errors->has('category_id'))
        <span class="help-block"><strong>{{ $errors->first('category_id') }}</strong></span>
    @endif
</div>

<!-- فیلد عنوان -->
<div class="form-group {{ $errors->has('title') ? ' has-error' : '' }}">
    {!! Form::label('title', 'عنوان') !!}
    {!! Form::text('title', null, ['class' => 'form-control']) !!}
    @if ($errors->has('title'))
        <span class="help-block"><strong>{{ $errors->first('title') }}</strong></span>
    @endif
</div>

<div>برای نوشتن میتوانید از <a href="https://wordtohtml.net/">ویرایش گر آنلاین</a> استفاده نمایید.</div>

<!-- فیلد متن پست -->
<div class="form-group {{ $errors->has('text') ? ' has-error' : '' }}">
    {!! Form::label('text', 'متن پست') !!}
    {!! Form::textarea('text', null, ['class' => 'form-control', 'id'=>'editor1']) !!}
    @if ($errors->has('text'))
        <span class="help-block"><strong>{{ $errors->first('text') }}</strong></span>
    @endif
</div>

<!-- فیلد تگ -->
<div class="form-group {{ $errors->has('tag_id[]') ? ' has-error' : '' }}">
    {!! Form::label('tag_id[]', 'تگ') !!}
    {!! Form::select('tag_id[]', $tags , (isset($post))?$post->tags:null , ['class' => 'form-control', 'multiple' => true ]) !!}
    @if ($errors->has('tag_id[]'))
        <span class="help-block"><strong>{{ $errors->first('tag_id[]') }}</strong></span>
    @endif
</div>


<div class="form-group">
    {!! Form::label('image', 'عکس پست') !!}
    {!! Form::file('image', ['class' => 'form-control']) !!}
</div>
{{--chape errore ax ziresh--}}

<div>
        <button type="submit" class="btn btn-success">انتشار</button>
        <a href="{{ route('post.index') }}" class="btn btn-danger">لغو</a>
        <a href="{{ route('tag.create') }}" class="btn btn-default">ساختن تگ جدید</a>
</div>