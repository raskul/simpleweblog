@extends('layouts.layout')
@section('content')

    <form action="{{ route('category.update', $category->id) }}" method="POST" role="form">
        {{ csrf_field() }}
        {{ method_field('PUT') }}
    	<legend>ویرایش دسته</legend>

    	<div class="form-group">
    		<label for=""></label>
    		<input type="text" class="form-control" name="name" id="" value="{{ $category->name }}" placeholder="Input...">
    	</div>
    	<button type="submit" class="btn btn-success">ثبت ویرایش</button>
    </form>

@endsection
