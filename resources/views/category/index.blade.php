@extends('layouts.layout')
@section('content')

    <a href="{{ route('category.create') }}" class="btn btn-primary">اضافه کردن دسته جدید</a>
    <hr>
    <table class="table table-hover">
        <thead>
        <tr>
            <th>گروه بندی نوشته ها:</th>
            <th>عملیات:</th>
        </tr>
        </thead>
        <tbody>

        @foreach($categories as $category)
            <tr>
                <td>{{ $category->name }}</td>
                <td>
                    <a href="{{ route('category.edit', $category->id) }}" class="btn btn-default btn-sm">
                        <span class="glyphicon glyphicon-edit"></span>
                    </a>
                    <form style="display: inline" action="{{ route('category.destroy', $category->id) }}" method="post">
                        {{ csrf_field() }}
                        {{ method_field('DELETE') }}
                        <button type="submit" class="btn btn-default btn-sm">
                            <span class="glyphicon glyphicon-trash"></span>
                        </button>
                    </form>
                </td>
            </tr>
        @endforeach

        </tbody>
    </table>

@endsection
