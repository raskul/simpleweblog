@extends('layouts.layout')
@section('content')

    <form action="{{ route('category.store') }}" method="POST" role="form">
        {{ csrf_field() }}
        <legend>افزودن دسته جدید:</legend>

        <div class="form-group">
            <input type="text" title="text" class="form-control" name="name" id="" placeholder="نام دسته...">
        </div>
        <div>
            <button type="submit" class="btn btn-success">ثبت</button>
            <a href="{{ route('category.index') }}" class="btn btn-danger">لغو</a>
        </div>
    </form>

@endsection
