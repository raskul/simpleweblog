@extends('layouts.layout')
@section('content')

    <form action="{{ route('tag.update', $tag->id) }}" method="POST" role="form">
        {{ csrf_field() }}
        {{ method_field('PUT') }}
    	<legend>ویرایش تگ</legend>

    	<div class="form-group">
    		<label for=""></label>
    		<input type="text" class="form-control" name="name" id="" value="{{ $tag->name }}" placeholder="Input...">
    	</div>
    	<button type="submit" class="btn btn-success">ثبت ویرایش</button>
    </form>

@endsection
