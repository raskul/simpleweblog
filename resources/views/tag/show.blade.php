@extends('layouts.layout')
@section('title', 'این یک وبلاگ ساده است!')
@section('content')

    @include('partials.header')

    <div id="posts">
        @foreach($posts as $post)
            <div id="post">
                <span id="title"><a href="{{ route('post.index') }}/{{ $post->id }}">{{ $post->title }}</a></span>
                <br>
                <a href="{{ route('post.index') }}/{{ $post->id }}"><img
                            src="{{ route('index.index') }}/{{ $post->image }}" alt=""></a>
                <br>
                <div id="text">{{ $post->text }}</div>
                <div>{{ $post->created_at }}</div>
            </div>
        @endforeach
    </div>

    <div align="center">
        {{--{{ $posts->render() }}--}}
    </div>


    @include('partials.footer')
@endsection