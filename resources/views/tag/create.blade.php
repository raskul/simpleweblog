@extends('layouts.layout')
@section('content')

    {{--<form action="{{ route('tag.store') }}" method="POST" role="form">--}}
        {{--{{ csrf_field() }}--}}

    {!! Form::open(['route'=>['tag.store']]) !!}
        <!-- فیلد نام تگ -->
        <div class="form-group {{ $errors->has('name') ? ' has-error' : '' }}">
            {!! Form::label('name', 'نام تگ:') !!}
            {!! Form::text('name', null, ['class' => 'form-control']) !!}
            @if ($errors->has('name'))
                <span class="help-block"><strong>{{ $errors->first('name') }}</strong></span>
            @endif
        </div>

        <div>
            <button type="submit" class="btn btn-success">ثبت</button>
            <a href="{{ route('tag.index') }}" class="btn btn-danger">لغو</a>
        </div>
    {!! Form::close() !!}

    {{--</form>--}}

@endsection
