@extends('layouts.layout')
@section('content')

    <a href="{{ route('tag.create') }}" class="btn btn-primary">اضافه کردن تگ جدید</a>
    <hr>
    <table class="table table-hover">
        <thead>
        <tr>
            <th>تگ نوشته ها:</th>
            <th>عملیات:</th>
        </tr>
        </thead>
        <tbody>

        @foreach($tags as $tag)
            <tr>
                <td>{{ $tag->name }}</td>
                <td>
                    <a href="{{ route('tag.edit', $tag->id) }}" class="btn btn-default btn-sm">
                        <span class=" glyphicon glyphicon-edit"></span>
                    </a>
                    <form style="display: inline" action="{{ route('tag.destroy', $tag->id) }}" method="post">
                        {{ csrf_field() }}
                        {{ method_field('DELETE') }}
                        <button type="submit" class="btn btn-default btn-sm">
                            <span class="glyphicon glyphicon-trash"></span>
                        </button>
                    </form>
                </td>
            </tr>
        @endforeach

        </tbody>
    </table>

@endsection
