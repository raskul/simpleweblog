<!DOCTYPE html>
<html lang="fa">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="این یک وبلاگ ساده است!">
    <meta name="author" content="Mohammad Reza">
    <title>@yield('title')</title>
    <title>این یک وبلاگ ساده است!</title>

<!-- Bootstrap -->
    <link href="{{ asset('bower_components/bootstrap/dist/css/bootstrap.min.css') }}" rel="stylesheet">
    <link href="{{ asset('bower_components/bootstrap-rtl/dist/css/bootstrap-rtl.min.css') }}" rel="stylesheet">
    <link href="{{ asset('css/style.css') }}" rel="stylesheet">
    <link href="{{ asset('css/user.css') }}" rel="stylesheet">
</head>
<body>
<div class="col-md-12">
    @stack('header')

    @yield('content')
</div>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
<script src="{{ asset('bower_components/bootstrap/dist/js/bootstrap.min.js') }}"></script>

    {{--<script src="https://cloud.tinymce.com/stable/tinymce.min.js"></script>--}}
    {{--<script>tinymce.init({ selector:'textarea' });</script>--}}
{{----}}
    @stack('footer')

    <script>$(window).load(function() {
        $("img").removeClass("preload");
    });</script>
</body>
</html>
