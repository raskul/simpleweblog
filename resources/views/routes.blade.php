@extends('layouts.layout')
@section('content')

    <div class="form-group">ادرس مدیریت پست ها:</div>
    <div class="form-group"><input type="text" value="http://simpleweblog.dev/post" class="form-control"></div>
    <div class="form-group">ادرس مدیریت دیدگاه ها:</div>
    <div class="form-group"><input type="text" value="http://simpleweblog.dev/comment" class="form-control"></div>
    <div class="form-group">ادرس مدیریت تگ ها:</div>
    <div class="form-group"><input type="text" value="http://simpleweblog.dev/tag" class="form-control"></div>
    <div class="form-group">ادرس مدیریت گروه بندی ها:</div>
    <div class="form-group"><input type="text" value="http://simpleweblog.dev/category" class="form-control"></div>
    <div class="form-group">ادرس صفحه اول:</div>
    <div class="form-group"><input type="text" value="http://simpleweblog.dev" class="form-control"></div>
    <div class="form-group">ادرس پست های دیگر:</div>
    <div class="form-group"><input type="text" value="http://simpleweblog.dev/post/11" class="form-control"></div>
    <div style="background-color: #98cbe8">
        <div class="form-group">ادرس لاگین:</div>
        <div class="form-group"><input type="text" value="http://simpleweblog.dev/login" class="form-control"></div>
        <div class="form-group">ادرس لاگ اوت:</div>
        <div class="form-group"><input type="text" value="http://simpleweblog.dev/logout" class="form-control"></div>
        <div class="form-group">ادرس خانه:</div>
        <div class="form-group"><input type="text" value="http://simpleweblog.dev/home" class="form-control"></div>
        <div class="form-group">ادرس ثبت نام:</div>
        <div class="form-group"><input type="text" value="http://simpleweblog.dev/register" class="form-control"></div>
        <div class="form-group">ادرس فراموشی رمز:</div>
        <div class="form-group"><input type="text" value="http://simpleweblog.dev/password/reset" class="form-control"></div>
    </div>>

@endsection