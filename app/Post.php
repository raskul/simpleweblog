<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Symfony\Component\Console\Tests\Command\CommandTest;

class Post extends Model
{

    protected $fillable =[
        'user_id', 'category_id', 'title', 'text', 'image'
    ];

    public function category()
    {
        return $this->belongsTo(Category::class);
    }

    public function tags()
    {
        return $this->belongsToMany(Tag::class);
    }

    public function comments()
    {
        return $this->hasMany(Comment::class);
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }

//    public static function owns($relatd , $post)
//    {
////        if (($relatd->user->id == $this->id) || ($relatd->user->admin == '1'))
////            return true;
//        return $relatd->id == $post->user_id;
//    }
}
