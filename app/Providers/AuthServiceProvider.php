<?php

namespace App\Providers;

use App\Policies\PostPolicy;
use App\Post;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;
use Illuminate\Support\Facades\Gate;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
//        \App\Tag::class => \App\Policies\TagCategoryPolicy::class,
//        \App\Tag::class => \App\Policies\TagPolicy::class,
//        'App\Tag'=> 'App\Policies\TagPolicy',
        Post::class => PostPolicy::class,

    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerPolicies();

//        Gate::define('edit-post', function ($user, $post) {
//            return Post::owns($user, $post);
//        });
//        Gate::define('edit-post', 'PostPolicy@edit');
//        if(Gate::resource('post', 'PostPolicy'))
//            dd('hello');
//            return true;



        Gate::define('edit-comment', function ($user, $comment) {
            if (($user->id == $comment->user_id) || ($user->admin == '1'))
                return true;
        });

        Gate::define('edit-tag-and-category', function ($user) {
            if ($user->admin == '1')
                return true;
        });

    }
}