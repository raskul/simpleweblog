<?php

namespace App\Http\Controllers;

use App\Policies\TagPolicy;
use App\Tag;
use Illuminate\Http\Request;
Use Gate;

class TagController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    function aclcheck()
    {
        if ( Gate::denies ('edit-tag-and-category' ) )
            abort (404,  'you are not admin' ) ;
    }

//    public function __construct()
//    {
//        if ( Gate::denies('edit-tag-and-category'))
//            abort(404,  'you are not admin');
//    }

    public function index()
    {
        $this->aclcheck();

//        if ( Gate::denies('edit-tag-and-category'))
//            abort(404,  'you are not admin');

//        if ( Gate::denies('update'))
//            abort(404,  'you are not admin');

        $tags = Tag::all();

        return view('tag.index', compact('tags'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $this->aclcheck();
        return view('tag.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        Tag::create($request->all());

        return redirect(route('tag.create'));
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Tag $tag
     * @return \Illuminate\Http\Response
     */
    public function show(Tag $tag)
    {
        $posts = $tag->posts()->paginate(6);

        return view('index', compact('posts'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Tag $tag
     * @return \Illuminate\Http\Response
     */
    public function edit(Tag $tag)
    {
        $this->aclcheck();
        return view('tag.edit', compact('tag'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \App\Tag $tag
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Tag $tag)
    {
        $tag->update($request->all());

        return redirect(route('tag.index'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Tag $tag
     * @return \Illuminate\Http\Response
     */
    public function destroy(Tag $tag)
    {
        $tag->delete();

        return redirect()->back();
    }
}
