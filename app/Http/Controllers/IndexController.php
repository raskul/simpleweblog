<?php

namespace App\Http\Controllers;

use App\Category;
use App\Post;
use App\Tag;
use DB;
use Illuminate\Http\Request;

class IndexController extends Controller
{
    public function home()
    {
        $posts = Post::orderBy('updated_at','desc')->paginate(6);
        return view('index', compact('posts', 'categories', 'tags'));
    }
}
