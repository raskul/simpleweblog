<?php

namespace App\Http\Controllers;

use App\Category;
use App\Comment;
use App\Http\Requests\PostRequest;
use App\Post;
use App\Tag;
use Gate;

class PostController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public $path = 'img/upload/';

    public function __construct()
    {
//        $this->middleware('auth', ['except' => ['show']]);
    }

    public function index()
    {
        $posts = Post::get();
//        Gate::denies('post');
//        Gate::allows('post');

        return view('post.index', compact('posts', 'comments', 'cats'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
//        $user = '1';

//        $this->authorize('create', post::class);//work

//        $this->authorize('create-post', "$user");
//        if(Gate::denies('create'))
//            abort(404, 'hihi');

        $categories = Category::pluck('name', 'id');
        $tags = Tag::pluck('name', 'id');
        return view('post.create2', compact('categories', 'tags'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(PostRequest $request)
    {
        $picname = $request->file('image')->getClientOriginalName();
//        $rand = rand(1000000 , 9000000);
//        $picname = $picname.$rand;
        $request->image->move($this->path, $picname);
        $picname = $this->path . $picname;
        $inputs = $request->all();
        $inputs['image'] = $picname;
        auth()->user()->Posts()->create($inputs)->tags()->sync($inputs['tag_id']);

        return redirect(route('post.create'));
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Post $post
     * @return \Illuminate\Http\Response
     */
    public function show(Post $post)
    {
        return view('post.show', compact('post'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Post $post
     * @return \Illuminate\Http\Response
     */
    public function edit(Post $post)
    {
//        $this->authorize('update', $post);//work

//        if(Gate::denies('edit-post', $post))
//            abort(404,'post male shoma nist') ;

        $categories = Category::pluck('name', 'id');
        $tags = Tag::pluck('name', 'id');
        return view('post.edit', compact('post', 'categories', 'tags'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \App\Post $post
     * @return \Illuminate\Http\Response
     */
    public function update(PostRequest $request, Post $post)
    {
        $picname = $request->file('image')->getClientOriginalName();
////        $rand = rand(1000000 , 9000000);
////        $picname = $picname.$rand;
        $request->image->move($this->path, $picname);
        $inputs = $request->all();
        $inputs['image'] = $this->path . $picname;
        $post->update($inputs);
        $post->tags()->sync($inputs['tag_id']);

        return redirect(route('post.index'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Post $post
     * @return \Illuminate\Http\Response
     */
    public function destroy(Post $post)
    {
        $post->delete();
        return redirect()->back();
    }
}
