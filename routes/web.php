<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'IndexController@home')->name('index.index');

Route::get('/tags/{tagname}',function($tagname){
    return $tagname;
});

Route::get('routes', function(){
    return view('routes');
});

Route::group(['middleware' => 'auth'], function(){
    Route::resource('category', 'CategoryController');
    Route::resource('tag', 'TagController');
});
Route::resource('comment', 'CommentController');

Route::resource('post', 'PostController');//->middleware('can:resource,post');


Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
Route::get('/welcome', function (){
    return view('welcome');
});
